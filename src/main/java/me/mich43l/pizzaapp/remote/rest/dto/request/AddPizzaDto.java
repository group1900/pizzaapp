package me.mich43l.pizzaapp.remote.rest.dto.request;

import me.mich43l.pizzaapp.remote.rest.dto.response.PizzaDto;
import me.mich43l.pizzaapp.remote.rest.dto.response.SizeDto;

import java.util.List;

public class AddPizzaDto {
    private PizzaDto name;
    private List<SizeDto> sizes;

    public AddPizzaDto() {
    }

    public AddPizzaDto(PizzaDto name, List<SizeDto> sizes) {
        this.name = name;
        this.sizes = sizes;
    }

    public PizzaDto getName() {
        return name;
    }

    public void setName(PizzaDto name) {
        this.name = name;
    }

    public List<SizeDto> getSizes() {
        return sizes;
    }

    public void setSizes(List<SizeDto> sizes) {
        this.sizes = sizes;
    }
}
