package me.mich43l.pizzaapp.remote.rest.dto.response;

import java.util.List;

public class PizzaDto {
    private Integer id;
    private String name;
    private List<SizeDto> sizeList;

    public PizzaDto() {}
    public PizzaDto(Integer id, String name, List<SizeDto> sizeList) {
        this.id = id;
        this.name = name;
        this.sizeList = sizeList;
        }

    public Integer getId() {
        return id;
        }
    public void setId(Integer id) {
        this.id = id;
        }
    public String getName() {
        return name;
        }
    public void setName(String name) {
        this.name = name;
        }
    public List<SizeDto> getSizeList() {
        return sizeList;
        }
    public void setSizeList(List<SizeDto> sizeList) {
        this.sizeList = sizeList;
        }
    }
