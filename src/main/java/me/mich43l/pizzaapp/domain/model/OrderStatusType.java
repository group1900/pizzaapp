package me.mich43l.pizzaapp.domain.model;

public enum OrderStatusType {
    NEW, IN_PROGRESS, READY, OUT_FOR_DELIVERY, DELIVERED, CANCELLED
}
